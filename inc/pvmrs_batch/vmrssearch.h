#ifndef _VMRSSEARCH_H_
#define _VMRSSEARCH_H_

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <unordered_set>
#include <vmrs/vmrssizes.h>
#include <vmrs/vmrs.h>
#include <vmrs/lookuptable.h>
#include <vmrs/postinglist.h>
#include <vmrs/arraylist.h>
#include <vmrs/qset.h>
#include <vmrs/trapdoor.h>

#include <omp.h>

using namespace std;
using namespace chrono;

using doc_list = unordered_map<ulong,float>;
using res_list = vector<pair<unsigned long, float>>;
using w_searches = unordered_map<string, vector<size_t>>;
using all_searches = unordered_map<string, w_searches>;
using all_results = vector<unordered_map<size_t,float>>;

class ArrayData {
 public:
  Zr yic;
  unsigned long fid;
  float score;
  bool beta;
  size_t c;
};

struct SearchItem {
    string w;
    string index;
    ArrayData* ar;
    string token;
};


class VmrsSearch {
 public:
  VmrsSearch(const VMRSSizes& vs, const string& pairing_file, const string& arrays_file, const string& lt_file, const string& qset_file, const string& res_file);

  void Search(const string& file_path);
  void save_results(const vector<res_list>& res);

  bool is_verbose () const;
  void set_verbose (bool verbose);
  ArrayData get_array_data(size_t addr, size_t c, string& fi_k3_w1);
  float find_token (string &w, string &token, ArrayData &ar);

  int get_threads () const;
  void set_threads (int threads);
  int get_repetitions () const;
  void set_repetitions (int repetitions);
  VMRSSizes vs;
  pLookupTable lt;
  QSet qs;
  ArrayList al;
  string res_file;
 private:
  Pairing* pairing;

  bool verbose;
  void logmsg(const string& msg);
  int repetitions;
  int threads;
  int batch_size;
 public:
  int get_batch_size () const;
  void set_batch_size (int batch_size);
 private:
  high_resolution_clock::time_point start;
};

#endif //_VMRSSEARCH_H_
