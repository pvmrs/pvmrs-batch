#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch.hpp>
#include <vmrs/keyset.h>
#include "../inc/pvmrs_batch/vmrssearch.h"

G1 getQsetKey(const ulong& fid, const string& w, const size_t& param, const size_t& zr, Pairing * p, const G1& g, const Keyset& ks)
{
  //FI k3 (wi)
  bitstring bs_w_k1 = hmac (ks.getPbcK3 (), w, zr);
  Zr zr_hash3w2 = from_bs(p, bs_w_k1);

  //xid = FI k1  (Fic)
  bitstring bs_hash1fid = hmac (ks.getPbcK1 (), to_string (fid), zr);
  Zr zr_hash1fid = from_bs (p, bs_hash1fid);

  return G1(g ^ (zr_hash1fid * zr_hash3w2));
}

TEST_CASE( "VmrsSearch") {
  using namespace std;
  using namespace vmrs;

  string pairing_file = "input/a.param";
  string keyset_file = "input/keyset.bin";
  string sizes_file = "input/sizes.txt";
  string qset_file = "input/qset.bin";
  string ar_file = "input/arraylist.bin";
  string lt_file = "input/lookuptable.bin";
  string hash_file = "input/ghash.bin";
  string res_file = "input/results.txt";

  VmrsSearch search(sizes_file, pairing_file, hash_file, ar_file, lt_file, qset_file, res_file);

  //load pairing
  FILE *sysParamFile = fopen(pairing_file.c_str(), "r");
  Pairing *p = new Pairing(sysParamFile);
  G1 g = G1(*p, false);

  //load keyset
  Keyset ks = Keyset::importKeys (keyset_file, 256, 256);

  //load sizes
  VMRSSizes vs;
  vs.importSizes (sizes_file);


  SECTION("pbc string conversion")
    {

      //conversion  string -> zr -> string
      string w = "asd";
      string w2 = "qwe";
      uint32_t c = 1;
      uint32_t fid = 1;

      //QSET w key
      bitstring bs_xid = hmac (ks.getPbcK1 (), to_string (fid), vs.zr_size);
      Zr zr_xid = from_bs (p, bs_xid);

      bitstring bs_fi_k3_w = hmac (ks.getPbcK3 (), w, vs.zr_size);
      Zr zr_fi_k3_w = from_bs (p, bs_fi_k3_w);

      G1 qset_key_w (g ^ (zr_fi_k3_w * zr_xid));
      string str_qset_key_w = qset_key_w.toString (false);

      //test string conversion

      G1 new_qset_key_w (*p, (const unsigned char *) str_qset_key_w.data (), bit_to_byte (vs.zp_size), false, 0);

      REQUIRE(new_qset_key_w == qset_key_w);
    }


    SECTION("token^yic calculation")
    {
      string w1 = "asd";
      string w2 = "qwe";
      uint32_t c = 1;
      uint32_t fid = 1;
      cout << "fid: " << fid << " w: " << w2 << " c: " << c << endl;

      //qset_key
      G1 qset_key = getQsetKey (fid, w2, vs.param_size, vs.zr_size, p, g, ks);
      g.dump(stdout, "\tqg", 16);
      qset_key.dump(stdout, "\tqset key", 16);

      //yic
      bitstring bs_xid = hmac (ks.getPbcK1 (), to_string (fid), vs.zr_size);
      Zr zr_xid = from_bs (p, bs_xid); //xid

      bitstring bs_z = hmac (ks.getPbcK1 (), w1 + to_string(c), vs.zr_size); //xid
      Zr zr_z = from_bs (p, bs_z); //z

      Zr yic(zr_xid / zr_z);
      cout << "fid: " << fid << " w: " << w1 << " c: " << c << endl;
      yic.dump(stdout, "\tyic", 16);

      //token
      bitstring bs_fi_k1_w1 = hmac(ks.getPbcK1 (), w1 + to_string(c), vs.zr_size);
      Zr zr_fi_k1_w1 = from_bs(p, bs_fi_k1_w1);

      bitstring bs_fi_k3_w2 = hmac(ks.getPbcK3 (), w2, vs.zr_size);
      Zr zr_fi_k3_w2 = from_bs(p, bs_fi_k3_w2);

      //token of qwe|f1
      G1 token (g ^ (zr_fi_k1_w1 * zr_fi_k3_w2));

      //G1 calculated_qset_key = token ^ yic;
      token^=yic;
      REQUIRE(token == qset_key);
    }
}