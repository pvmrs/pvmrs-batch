#include "vmrssearch.h"
#include <charconv>
#include <iterator>

bool sortbysec(const pair<unsigned long,float> &a, const pair<unsigned long,float> &b)
{
  return (a.second > b.second);
}

VmrsSearch::VmrsSearch (const VMRSSizes& vs, const string& pairing_file, const string& arrays_file, const string& lt_file, const string& qset_file, const string& res_file)
  : start(std::chrono::high_resolution_clock::now()), vs(vs), lt(vs, lt_file), res_file(res_file), verbose(true), threads(1), repetitions(1), batch_size(1)
{
  //Pairing
  FILE *sysParamFile = fopen(pairing_file.c_str(), "r");
  if (sysParamFile == nullptr) {
      cerr <<"Can't open the parameter file " << pairing_file << "\n";
      exit(1);
    }

  logmsg("Importing PBC parameters.");
  this->pairing = new Pairing(sysParamFile);

  logmsg("Importing ArrayList.");
  al = importArrayList (arrays_file, vs.max, vs.h1_size, vs.param_size, vs.array_size);
  logmsg("Importing QSet.");
  qs = importQSet(qset_file, vs.zp_size, vs.score_size);

  logmsg("VMRS Search intantiated.");
}

void VmrsSearch::Search (const string& file_path)
{
  ifstream ifs(file_path);
  boost::archive::text_iarchive ia(ifs);

  vector<Trapdoor> vec_tr;
  ia >> vec_tr;

  vector<res_list> res(vec_tr.size());

  size_t search_count = 0;

  //logmsg("Search file: " + file_path);

  size_t vec_size = vec_tr.size();
  size_t it_size = ceil((double)vec_size/ (double) batch_size);

  for (int j = 0; j < it_size;j++)
    {
      //logmsg("Start batch search " + to_string(vec_size) + " searches.");
      auto search_start = high_resolution_clock::now();
      //auto search_start1 = high_resolution_clock::now();

      size_t vec_start = j*batch_size;
      size_t vec_end = (j+1)*batch_size > vec_size ? vec_size : (j+1)*batch_size;
      size_t cur_vec_size = vec_end - vec_start;

      //w1+cur_w+c = score
      unordered_map <string, float> search_map;
      search_map.reserve(cur_vec_size*vs.max);

      vector<SearchItem*> search_vec;

      //search index, fid, score
      vector<vector<pair<size_t, float>>> results;
      results.reserve(cur_vec_size);
      int max_exp = 0;

      //count W1s and Fs
      for (size_t k = vec_start; k < vec_end; k++)
        {
          Trapdoor *tr = &vec_tr[k];

          string w1 = tr->first_w;
          size_t files_w1 = 0;
          size_t other_size = 0;

          if (lt.find (tr->first_w) == lt.end ())
            {
              vector<pair<size_t, float>> temp;
              results.push_back(temp);
              continue;
            }

          pLookupTable::pLookupTableItem ltItem = lt.get (tr->first_w);
          files_w1 = ltItem.size;
          other_size = tr->other_w.size ();

          //for each file in w1
          vector<pair<size_t, float>> cur_scores;
          cur_scores.reserve(files_w1);
          //logmsg("Search " + to_string(k) + "Files in w1: " + to_string(files_w1));
          for (size_t c = 1; c <= files_w1; ++c)
            {

              //make ArrayData
              string ltaddr = ltItem.addr;
              bitstring f_k2_w1(tr->f_k2_w1, vs.f_size);
              bitstring enc_addr(ltaddr, vs.f_size);
              size_t addr = (f_k2_w1^enc_addr).to_ulong ();
              auto *cur_ar = new ArrayData;
              *cur_ar = get_array_data (addr, c, tr->fi_k3_w1);

              //store w1 and fid in results
              cur_scores.emplace_back(cur_ar->fid, cur_ar->score);
              search_map[w1 + to_string(cur_ar->fid)] = cur_ar->score;
              //logmsg("Fid: " + to_string(cur_ar->fid) + " score: " + to_string(cur_ar->score));

              for (size_t i = 1; i <= other_size; i++)
                {
                  //max_exp++;
                  string index = tr->other_w[i + 1] + to_string(cur_ar->fid);
                  if (search_map.find(index) == search_map.end())
                    {
                      auto* si = new SearchItem;
                      si->token = tr->tokens.at({c, i+1});
                      si->w = tr->other_w[i + 1];
                      si->ar = cur_ar;
                      si->index = index;
                      search_vec.push_back(si);
                      search_map[index] = 0;
                    }
                }
            }

          results.push_back(cur_scores);
        }
      //auto search_end1 = high_resolution_clock::now();
      //auto search_dur1 = duration_cast<nanoseconds>(search_end1 - search_start1);
      //logmsg(to_string(j) + ") Primeira parte duration " + to_string(search_dur1.count()));

      //compute all scores
      //search_start1 = high_resolution_clock::now();
      size_t search_size = search_vec.size();
      #pragma omp parallel for num_threads(threads) default(none) shared(search_size, search_vec,search_map)
      for ( size_t i = 0; i < search_size; i++)
        {
          SearchItem *si = search_vec[i];
          search_map[si->index] = find_token (si->w, si->token, *(si->ar));
        }
      //search_end1 = high_resolution_clock::now();
      //search_dur1 = duration_cast<nanoseconds>(search_end1 - search_start1);
      //logmsg(to_string(j) + ") Segunda parte duration " + to_string(search_dur1.count()));
      //logmsg("Exponenciacoes: " + to_string(search_size) + "/" + to_string(max_exp));


      //search_start1 = high_resolution_clock::now();
      for (auto & it : search_vec)
        {
          delete it;
        }
      search_vec.clear();

      //reduce scores for each file in each search
      logmsg("Aggregating scores");
      #pragma omp parallel for num_threads(threads) default(none) shared(vec_start, vec_end, vec_tr, results, res, j, search_map)
      for (size_t k = 0; k < results.size(); k++)
        {
          Trapdoor* tr = &vec_tr[k+vec_start];
          size_t fid_size = results[k].size();

          for (size_t c = 0; c < fid_size; ++c)
            {
              for (auto & [i, cur_w] : tr->other_w)
                {
                  results[k][c].second += search_map[cur_w + to_string(results[k][c].first)];
                }
            }

          sort(results[k].begin(), results[k].end(), sortbysec);

          if (results[k].size() > tr->k)
            {
              results[k].resize (tr->k);
            }

          //size_t cur_search = j*vec_size + k;
          res[k+vec_start] = results[k];
        }

      auto search_end = high_resolution_clock::now();

      auto search_dur = duration_cast<nanoseconds>(search_end - search_start);
      //search_end1 = high_resolution_clock::now();
      //search_dur1 = duration_cast<nanoseconds>(search_end1 - search_start1);
      //logmsg(to_string(j) + ") Terceira parte duration " + to_string(search_dur1.count()));
      logmsg(to_string(j) + ") Batch duration " + to_string(search_dur.count()));
    }

  save_results(res);
}

void VmrsSearch::save_results (const vector<res_list>& res)
{
  ofstream ifres(this->res_file, std::ios_base::app);
  if (!ifres.good())
    {
      cout <<"Can't open the results file " << res_file << "\n";
    }

    for (size_t i = 0; i < res.size(); i++)
      {
        if (res[i].empty ())
        {
          ifres << "Search " << i+1 << ": no files found" << endl;
        }
        else
          {
            ifres << "Search " << i+1 << endl;
            for (const auto& item : res[i])
              {
                ifres << "\tFile ID: " << item.first << " Score: " << item.second << endl;
              }
          }

      }
    ifres.close ();

}
bool VmrsSearch::is_verbose () const
{
  return verbose;
}
void VmrsSearch::set_verbose (bool verbose)
{
  VmrsSearch::verbose = verbose;
}
void VmrsSearch::logmsg (const string &msg)
{
  if (verbose) {
      auto stop = std::chrono::high_resolution_clock::now();
      auto duration = duration_cast<nanoseconds>(stop - start);
      cout << duration.count() << "ns: " << msg << endl;
    }
}
int VmrsSearch::get_repetitions () const
{
  return repetitions;
}
void VmrsSearch::set_repetitions (int repetitions)
{
  VmrsSearch::repetitions = repetitions;
}

int VmrsSearch::get_threads () const
{
  return threads;
}
void VmrsSearch::set_threads (int threads)
{
  VmrsSearch::threads = threads;
}

ArrayData VmrsSearch::get_array_data(size_t addr, size_t c, string& fi_k3_w1) {
  bitstring hash_h1 = vmrs::hash (fi_k3_w1 + al[addr][c-1].second, vs.h1_size);
  bitstring enc_data (al[addr][c-1].first, vs.h1_size);
  bitstring ar_data = enc_data ^hash_h1;

  ArrayData ar;
  string stryic = ar_data.substring (vs.fid_size + vs.score_size, vs.zr_size).to_string ();
  ar.yic = Zr(*pairing, (const unsigned char *)stryic.data(), bit_to_byte (vs.zr_size));
  ar.fid = ar_data.substring (vs.fid_size).to_ulong ();
  ar.score = ar_data.substring (vs.fid_size, vs.score_size).to_float ();
  ar.beta = ar_data[ar_data.size() - 1];
  ar.c = c;

  return ar;
}

float VmrsSearch::find_token (string &w, string &token, ArrayData &ar) {
  G1 gtoken(*pairing, (const unsigned char *)token.data(), bit_to_byte (vs.zp_size), false, 0);
  gtoken ^= ar.yic;

  //search file c with word i in QSet
  auto found = qs.find (gtoken.toString (false));
  if (found != qs.end())
    {
      //logmsg("File " + to_string(fid) + " found in W" + to_string(i));
      bitstring enc_tfidf(found->second);
      bitstring hash_tfidf = vmrs::hash(w + to_string(ar.fid), vs.score_size);
      return (enc_tfidf^hash_tfidf).to_float ();
    }
  else
    {
      return 0.0;
    }
}
int VmrsSearch::get_batch_size () const
{
  return batch_size;
}
void VmrsSearch::set_batch_size (int batch_size)
{
  VmrsSearch::batch_size = batch_size;
}
